class PeoplesController < ApplicationController
  def index
    @peoples = Person.paginate(:page => params[:page], :per_page => 5)
  end
end
